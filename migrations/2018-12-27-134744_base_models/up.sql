BEGIN;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE company (
  company_id UUID PRIMARY KEY DEFAULT uuid_generate_v4 (),
  name TEXT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
  updated_at TIMESTAMP
);

CREATE TABLE department (
  department_id UUID PRIMARY KEY DEFAULT uuid_generate_v4 (),
  name TEXT NOT NULL,
  company_id UUID NOT NULL REFERENCES company ON DELETE RESTRICT,
  created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
  updated_at TIMESTAMP
);

CREATE TABLE "user" (
  user_id UUID PRIMARY KEY DEFAULT uuid_generate_v4 (),
  name TEXT NOT NULL,
  email TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL,
  roles TEXT [] NOT NULL,
  company_id UUID REFERENCES company ON DELETE RESTRICT,
  created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
  updated_at TIMESTAMP
);

CREATE TABLE department_users (
  department_id UUID REFERENCES department ON DELETE RESTRICT,
  user_id UUID REFERENCES "user" ON DELETE RESTRICT,
  PRIMARY KEY (department_id, user_id)
);


COMMIT;

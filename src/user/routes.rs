
use actix_web::{HttpRequest, HttpResponse, Json};
use crate::shared::{AppState, ApiError};
use crate::user::{User, CreateUser};

pub fn find_all(req: &HttpRequest<AppState>) -> Result<HttpResponse, ApiError> {
  let conn = &req.state().db.connection()?;
  let users = User::find_all(conn)?;
  json_ok!({"users": users})
}

pub fn create(req: HttpRequest<AppState>, user: Json<CreateUser>) -> Result<HttpResponse, ApiError> {
  let conn = &req.state().db.connection()?;
  let user = User::create(conn, user.into_inner())?;
  json_ok!({"user": user})
}

use diesel::prelude::*;
use chrono::prelude::*;
use uuid::Uuid;
use crate::schema::user;
use crate::shared::crypto::{encrypt_password, verify_password};
use crate::shared::ApiError;

#[derive(Debug, Serialize, Deserialize)]
pub struct CreateUser {
  pub name: String,
  pub email: String,
  pub password: String,
}

#[table_name="user"]
#[derive(Debug, Clone, Serialize, Deserialize, Insertable, Queryable)]
pub struct User {
  pub user_id: Uuid,
  pub name: String,
  pub email: String,
  pub password: String,
  pub roles: Vec<String>,
  pub company_id: Option<Uuid>,
  pub created_at: NaiveDateTime,
  pub updated_at: Option<NaiveDateTime>,
}

impl User {
  pub fn verify_password(&self, password: String) -> Result<(), ApiError> {
    verify_password(&self.user_id, &self.password, &password)
  }

  pub fn find_all(conn: &PgConnection) -> Result<Vec<User>, ApiError> {
    let result = user::table.load::<User>(conn)?;

    Ok(result)
  }

  pub fn find(conn: &PgConnection, id: Uuid) -> Result<User, ApiError> {
    let result = user::table.find(id).get_result::<User>(conn)?;

    Ok(result)
  }

  pub fn find_by_email(conn: &PgConnection, email: String) -> Result<User, ApiError> {
    let result = user::table.filter(user::email.eq(email)).get_result::<User>(conn)?;

    Ok(result)
  }

  pub fn create(conn: &PgConnection, user: CreateUser) -> Result<User, ApiError> {
    let user = User::from(user);
    let result = diesel::insert_into(user::table)
      .values(&user)
      .get_result::<User>(conn)?;

    Ok(result)
  }
}

impl From<CreateUser> for User {
  fn from(user: CreateUser) -> User {
    let user_id = Uuid::new_v4();
    let password = encrypt_password(&user_id, &user.password);
    User {
      user_id,
      password,
      name: user.name,
      email: user.email,
      roles: vec![String::from("user")],
      company_id: None,
      created_at: Utc::now().naive_utc(),
      updated_at: None
    }
  }
}

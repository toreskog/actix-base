
mod model;
mod routes;

use actix_web::App;
use actix_web::http::Method;
use crate::shared::AppState;

pub use self::model::*;
use self::routes::*;

pub fn init(app: App<AppState>) -> App<AppState> {
  app
    .resource("/companies", |r| {
      r.method(Method::POST).with(create);
    })
}

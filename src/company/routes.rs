
use actix_web::{HttpRequest, HttpResponse, Json};
use crate::shared::{AppState, ApiError};
use crate::company::CreateCompanyWithUser;

pub fn create(req: HttpRequest<AppState>, create_company: Json<CreateCompanyWithUser>) -> Result<HttpResponse, ApiError> {
  let conn = &req.state().db.connection()?;
  let create_company = create_company.into_inner();

  let (company, user) = create_company.execute(conn)?;
  json_ok!({
    "company": company,
    "user": user,
  })
}


use diesel::prelude::*;
use chrono::prelude::*;
use uuid::Uuid;
use crate::schema::company;
use crate::shared::ApiError;
use crate::user::{User, CreateUser};

#[derive(Debug, Serialize, Deserialize)]
pub struct CreateCompanyWithUser {
  pub company: CreateCompany,
  pub user: CreateUser,
}

impl CreateCompanyWithUser {
  pub fn execute(self, conn: &PgConnection) -> Result<(Company, User), ApiError> {
    let company = self.company.execute(conn)?;
    let user = User::create(conn, self.user)?;

    Ok((company, user))
  }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CreateCompany {
  pub name: String,
}

impl CreateCompany {
  pub fn execute(self, conn: &PgConnection) -> Result<Company, ApiError> {
    let company = Company::from(self);
    let result = diesel::insert_into(company::table)
      .values(&company)
      .get_result::<Company>(conn)?;

    Ok(result)
  }
}

#[table_name="company"]
#[derive(Debug, Serialize, Deserialize, Insertable, Queryable)]
pub struct Company {
  pub company_id: Uuid,
  pub name: String,
  pub created_at: NaiveDateTime,
  pub updated_at: Option<NaiveDateTime>,
}

impl From<CreateCompany> for Company {
  fn from (company: CreateCompany) -> Company {
    Company {
      company_id: Uuid::new_v4(),
      name: company.name,
      created_at: Utc::now().naive_utc(),
      updated_at: None,
    }
  }
}

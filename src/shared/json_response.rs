
#[macro_export]
macro_rules! json_ok {
  ($($json:tt)+) => {
    Ok(
      HttpResponse::build(actix_web::http::StatusCode::OK)
        .content_type("application/json; charset=utf-8")
        .body(json!($($json)+).to_string())
    )
  };
}

#[macro_export]
macro_rules! json_created {
  ($($json:tt)+) => {
    Ok(
      HttpResponse::build(actix_web::http::StatusCode::CREATED)
        .content_type("application/json; charset=utf-8")
        .body(json!($($json)+).to_string())
    )
  };
}

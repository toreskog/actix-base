
#[macro_use] mod json_response;
mod api_error;
pub mod crypto;

use crate::db::DbPool;
pub use self::api_error::ApiError;

pub struct AppState {
  pub db: DbPool,
}

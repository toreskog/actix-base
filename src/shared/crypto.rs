#![allow(dead_code)]

use uuid::Uuid;
use ring::{digest, pbkdf2};
use data_encoding::HEXUPPER;
use crate::shared::ApiError;
//use rocket::http::Status;

static DIGEST_ALG: &'static digest::Algorithm = &digest::SHA256;
const HASH_LEN: usize = digest::SHA256_OUTPUT_LEN;
const PBKDF2_ITERATIONS: u32 = 60_000;
const SALT_COMPONENT: [u8; 16] = [
  0xd6, 0x26, 0x98, 0xda, 0xf4, 0xdc, 0x50, 0x52,
  0x24, 0xf2, 0x27, 0xd1, 0xfe, 0x39, 0x01, 0x8a
];
pub type Hash = [u8; HASH_LEN];


pub fn encrypt_password(user_id: &Uuid, password: &String) -> String {
  let salt = salt(user_id);
  let mut hash: Hash = [0u8; HASH_LEN];

  pbkdf2::derive(
    DIGEST_ALG,
    PBKDF2_ITERATIONS,
    &salt,
    password.as_bytes(),
    &mut hash,
  );

  HEXUPPER.encode(&hash)
}

pub fn verify_password(user_id: &Uuid, actual_password: &String, attempted_password: &String)
                       -> Result<(), ApiError> {
  let actual_password: Vec<u8> = match HEXUPPER.decode(actual_password.as_bytes()) {
    Ok(res) => Ok(res),
    Err(_) => Err(ApiError::new(500, String::from("Failed to decode password"))),
  }?;

  let salt = salt(user_id);
  match pbkdf2::verify(
    DIGEST_ALG,
    PBKDF2_ITERATIONS,
    &salt,
    attempted_password.as_bytes(),
    &actual_password,
  ) {
    Ok(_) => Ok(()),
    Err(_) => Err(ApiError::new(401, String::from("Wrong password"))),
  }
}

fn salt(user_id: &Uuid) -> Vec<u8> {
  let user_id = user_id.as_bytes();
  let salt_len = SALT_COMPONENT.len() + user_id.len();

  let mut salt = Vec::with_capacity(salt_len);
  salt.extend(SALT_COMPONENT.as_ref());
  salt.extend(user_id);
  salt
}

use std::fmt;
use std::error::Error as StdError;
use diesel::result::Error as DieselError;
use actix_web::{HttpResponse, ResponseError};
use actix_web::http::StatusCode;

#[derive(Debug)]
pub struct ApiError {
  pub status_code: u16,
  pub message: String,
}

impl ApiError {
  pub fn new(status_code: u16, message: String) -> ApiError {
    ApiError{status_code, message}
  }
}

impl From<DieselError> for ApiError {
  fn from(error: DieselError) -> ApiError {
    match error {
      DieselError::DatabaseError(_, err) => ApiError::new(409, err.message().to_string()),
      DieselError::NotFound => ApiError::new(404, String::from("Record not found")),
      err => {
        println!("Diesel error: {}", err);
        ApiError::new(500, String::from("Internal error"))
      }
    }
  }
}

impl fmt::Display for ApiError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    f.write_str(self.message.as_str())
  }
}

impl StdError for ApiError {
  fn description(&self) -> &str {
    self.message.as_str()
  }
}

impl ResponseError for ApiError {
  fn error_response(&self) -> HttpResponse {
    let status_code = match StatusCode::from_u16(self.status_code) {
      Ok(status_code) => status_code,
      Err(_) => StatusCode::INTERNAL_SERVER_ERROR,
    };

    HttpResponse::build(status_code)
      .content_type("application/json")
      .body(json!({"message": self.message}).to_string())
  }
}

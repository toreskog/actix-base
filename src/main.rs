#![allow(proc_macro_derive_resolution_fallback)]

extern crate actix_web;
extern crate actix_redis;
extern crate chrono;
extern crate regex;
extern crate dotenv;
#[macro_use] extern crate dotenv_codegen;
#[macro_use] extern crate serde_json;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate diesel;
extern crate jsonwebtoken as jwt;

mod schema;
#[macro_use] mod shared;
mod db;
mod user;
mod auth;
mod company;

use std::env;
use actix_web::{actix, server, App};
use actix_web::middleware::session::SessionStorage;
use actix_redis::RedisSessionBackend;
use crate::db::DbPool;
use crate::shared::AppState;
use crate::auth::Bouncer;

static APP: &'static str = dotenv!("APP");
static HOST: &'static str = dotenv!("HOST");
static PORT: &'static str = dotenv!("PORT");

fn main() {
    env::set_var("RUST_LOG", "actix_web=debug");
    let sys = actix::System::new(APP);

    let app = || {
        let db = DbPool::connect();
        let mut app = App::with_state(AppState{db: db.clone()})
          .prefix("/api")
          .middleware(SessionStorage::new(
              RedisSessionBackend::new("0.0.0.0:6379", &[0; 32])
          ))
          .middleware(Bouncer::new(db));

        app = user::init(app);
        app = auth::init(app);
        app = company::init(app);

        app
    };

    server::new(app)
      .bind(format!("{}:{}", HOST, PORT))
      .expect(format!("Can not bind to {}:{}", HOST, PORT).as_str())
      .start();

    println!("Starting http server: {}:{}", HOST, PORT);
    let _ = sys.run();
}

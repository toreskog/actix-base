use uuid::Uuid;
use crate::user::User;
use chrono::{Local, Duration};

#[derive(Debug, Serialize, Deserialize)]
pub struct Credentials {
  pub email: String,
  pub password: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
  pub user_id: Uuid,
  pub name: String,
  pub email: String,
  pub iat: i64,
  pub exp: i64,
}

impl From<User> for Claims {
  fn from(user: User) -> Claims {
    let now = Local::now();

    Claims{
      user_id: user.user_id,
      name: user.name,
      email: user.email,
      iat: now.timestamp(),
      exp: (now + Duration::hours(1)).timestamp(),
    }
  }
}

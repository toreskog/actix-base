
mod api;
mod model;
mod routes;
mod bouncer;

use actix_web::App;
use actix_web::http::Method;
use crate::shared::AppState;

pub use self::api::Api;
pub use self::model::{Credentials, Claims};
use self::routes::*;
pub use self::bouncer::Bouncer;

pub fn init(app: App<AppState>) -> App<AppState> {
  app
    .resource("/sign-in", |r| { r.method(Method::POST).with(sign_in) })
    .resource("/sign-out", |r| { r.method(Method::POST).with(sign_out) })
}

#![allow(dead_code)]
#![allow(unused_assignments)]

use uuid::Uuid;
use regex::Regex;
use actix_web::{HttpRequest, Result};
use actix_web::middleware::{Middleware, Started};
use actix_web::middleware::session::{RequestSession};
use crate::db::DbPool;
use crate::shared::ApiError;
use crate::user::User;

pub struct Bouncer {
  db: DbPool,
  policies: Vec<Policy>,
}

#[derive(Debug)]
struct Policy {
  role: &'static str,
  path: Regex,
  methods: Vec<&'static str>,
}

impl Bouncer {
  pub fn new(db: DbPool) -> Bouncer {
    let policies = vec![
      Policy{role: "anonymous", path: Regex::new(r"^/api/sign-in$").unwrap(), methods: vec!["POST"]},
      Policy{role: "anonymous", path: Regex::new(r"^/api/users$").unwrap(), methods: vec!["POST"]},
      Policy{role: "anonymous", path: Regex::new(r"^/api/companies$").unwrap(), methods: vec!["POST"]},

      Policy{role: "user", path: Regex::new(r"^/api/sign-out$").unwrap(), methods: vec!["POST"]},

      Policy{role: "admin", path: Regex::new(r"^/api/users$").unwrap(), methods: vec!["GET"]},
    ];
    Bouncer{db, policies}
  }
}

impl<S> Middleware<S> for Bouncer {

  fn start(&self, req: &HttpRequest<S>) -> Result<Started> {
    let user_id = req.session().get::<Uuid>("user_id")?;

    let user_roles = match user_id {
      Some(user_id) => {
        let conn = &self.db.connection()?;
        let user = User::find(conn, user_id)
          .map_err(|err| {
            match err.status_code {
              404 => {
                req.session().clear();
                ApiError::new(401, String::from("Signed in user seems to no longer exist"))
              },
              _ => err
            }
          })?;
        user.roles
      }
      None => {
        vec![String::from("anonymous")]
      }
    };

    let req_path = req.path();
    let req_method = req.method();

    let policy = self.policies.iter().find(|policy| {
      user_roles.iter().any(|role| { role as &str == policy.role }) &&
      policy.path.is_match(req_path) &&
      policy.methods.iter().any(|method| { method == req_method })
    });

    match policy {
      Some(_) => Ok(Started::Done),
      None => {
        match user_id == None {
          true => Err(ApiError::new(401, String::from("You need to be logged in to access this route")).into()),
          false => Err(ApiError::new(401, String::from("You do not have permissions to access this route")).into()),
        }
      },
    }
  }
}

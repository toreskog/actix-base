use actix_web::{HttpRequest, HttpResponse, Json};
use actix_web::middleware::session::{RequestSession};
use crate::shared::{AppState, ApiError};
use crate::auth::{Api, Credentials};

#[allow(dead_code)]
pub fn token(req: HttpRequest<AppState>, credentials: Json<Credentials>) -> Result<HttpResponse, ApiError> {
  let conn = &req.state().db.connection()?;
  let token = Api::get_token(conn, credentials.into_inner())?;
  json_ok!({
    "message": "Successfully created token",
    "token": token,
  })
}

pub fn sign_in(req: HttpRequest<AppState>, credentials: Json<Credentials>) -> Result<HttpResponse, ApiError> {
  let conn = &req.state().db.connection()?;
  let user = Api::sign_in(conn, req.session(), credentials.into_inner())?;

  json_ok!({
    "message": "Successfully signed in",
    "user": user,
  })
}

pub fn sign_out(req: HttpRequest<AppState>) -> Result<HttpResponse, ApiError> {
  req.session().clear();
  json_ok!({
     "message": "Successfully signed out",
  })
}

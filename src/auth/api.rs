
use diesel::prelude::*;
use actix_web::middleware::session::{Session};
use jwt::{encode, decode, Header, Validation, Algorithm};
use crate::shared::ApiError;
use crate::auth::{Credentials, Claims};
use crate::user::User;

static TOKEN_SECRET: &'static str = dotenv!("TOKEN_SECRET");

pub struct Api;

impl Api {

  #[allow(dead_code)]
  pub fn get_token(conn: &PgConnection, credentials: Credentials) -> Result<String, ApiError> {
    let user = User::find_by_email(conn, credentials.email)?;
    user.verify_password(credentials.password)?;

    let header = Header::new(Algorithm::HS256);
    let claims = Claims::from(user);
    let token = encode(&header, &claims, TOKEN_SECRET.as_ref());

    match token {
      Ok(token) => Ok(token),
      Err(err) => Err(ApiError::new(500, err.to_string())),
    }
  }

  #[allow(dead_code)]
  pub fn validate_token(token: &str) -> Result<Claims, ApiError> {
    let token = token.trim_start_matches("Bearer ");
    let token_data = decode::<Claims>(token, TOKEN_SECRET.as_ref(), &Validation::new(Algorithm::HS256))
      .map_err(|_| { ApiError::new(401, String::from("Invalid token")) })?;

    Ok(token_data.claims)
  }

  pub fn sign_in(conn: &PgConnection, session: Session, credentials: Credentials) -> Result<User, ApiError> {
    let user = User::find_by_email(conn, credentials.email)
      .map_err(|err| {
        match err.status_code {
          404 => ApiError::new(401, String::from("User not found")),
          _ => err,
        }
      })?;
    user.verify_password(credentials.password)?;
    session.set("user_id", user.user_id)
      .map_err(|_| { ApiError::new(500, String::from("Failed to store user id in session")) })?;

    Ok(user)
  }
}

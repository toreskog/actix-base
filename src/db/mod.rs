use diesel::pg::PgConnection;
use diesel::r2d2::ConnectionManager;
use r2d2;
use crate::shared::ApiError;

static DATABASE_URL: &'static str = dotenv!("DATABASE_URL");

pub struct DbPool(
  pub r2d2::Pool<ConnectionManager<PgConnection>>
);

impl DbPool {
  pub fn connect() -> DbPool {
    let manager = ConnectionManager::<PgConnection>::new(DATABASE_URL);
    let pool = r2d2::Pool::builder().build(manager).expect("Failed to create database pool");
    DbPool(pool)
  }

  pub fn connection(&self) -> Result<r2d2::PooledConnection<ConnectionManager<PgConnection>>, ApiError> {
    match self.0.get() {
      Ok(conn) => Ok(conn),
      Err(_) => Err(ApiError::new(500, String::from("Failed to get db connection"))),
    }
  }
}

impl Clone for DbPool {
  fn clone(&self) -> DbPool {
    let pool_manager = self.0.clone();
    DbPool(pool_manager)
  }
}

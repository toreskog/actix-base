table! {
    company (company_id) {
        company_id -> Uuid,
        name -> Text,
        created_at -> Timestamp,
        updated_at -> Nullable<Timestamp>,
    }
}

table! {
    department (department_id) {
        department_id -> Uuid,
        name -> Text,
        company_id -> Uuid,
        created_at -> Timestamp,
        updated_at -> Nullable<Timestamp>,
    }
}

table! {
    department_users (department_id, user_id) {
        department_id -> Uuid,
        user_id -> Uuid,
    }
}

table! {
    user (user_id) {
        user_id -> Uuid,
        name -> Text,
        email -> Text,
        password -> Text,
        roles -> Array<Text>,
        company_id -> Nullable<Uuid>,
        created_at -> Timestamp,
        updated_at -> Nullable<Timestamp>,
    }
}

joinable!(department -> company (company_id));
joinable!(department_users -> department (department_id));
joinable!(department_users -> user (user_id));
joinable!(user -> company (company_id));

allow_tables_to_appear_in_same_query!(
    company,
    department,
    department_users,
    user,
);
